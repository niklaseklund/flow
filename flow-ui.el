;;; flow-core.el --- Support representing flow entities in tablists -*- lexical-binding: t; -*-

;; Copyright (C) 2019-2020 Niklas Carlsson, David Friberg, Jonathan Karlsson

;; Authors: Niklas Carlsson <niklas.carlsson@posteo.net>,
;;          David Friberg <david.n.friberg@gmail.com>,
;;          Jonathan Karlsson <karlsjona@gmail.com>
;; URL: https://gitlab.com/niklascarlsson/gocd/gocd.el
;; Version: 0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This file provides some ui common ground for all flow user-interfaces.

;;; Code:

;;;; Requirements
(require 'flow-core)
(require 'navigel)

;; For `navigel-open'
(declare-function flow-open-commit-message "flow" (patch))
(declare-function flow-open-job "flow" (job))

;;;; Pipeline

(defun flow-open (entity target)
  "Open a buffer displaying ENTITY and goto TARGET."
  (let ((navigel-app 'flow))
    (navigel-open entity target)))

(navigel-method flow navigel-entity-buffer (entity)
  "Return a string representing ENTITY in the buffer's name."
  (flow-buffer-name entity))

(cl-defgeneric flow-buffer-name (entity)
  "Return a string representation ENTITY in the buffer's name.")

(cl-defmethod flow-buffer-name (_entity)
  "Return a string representation ENTITY in the buffer's name."
  (format "*flow-pipelines*"))

(cl-defmethod flow-buffer-name ((_entity flow-pipeline))
  "Return a string representation ENTITY in the buffer's name."
  (format "*flow-instances*"))

(defun flow-list-pipelines ()
  "List monitored flow pipelines."
  (interactive)
  (flow--fetch-pipelines)
  (flow-open flow-pipelines-data (seq-elt flow-pipelines-data 0)))

(navigel-method flow navigel-open (_entity _callback)
  "Call CALLBACK with the most recent instances."
  (flow--cleanup-buffer "*flow-patches*")
  (flow--cleanup-buffer "*flow-jobs*")
  (flow--cleanup-buffer "*flow-instances*")
  (flow--cleanup-buffer "*flow-main*")
  (cl-call-next-method))

(navigel-method flow navigel-children (entity callback)
  "Call CALLBACK with the monitored pipelines as input."
  (funcall callback entity))

(navigel-method flow navigel-tablist-format (_entity)
  "Return a vector specifying columns to display pipelines's children."
  (vector (list "Pipeline" 30 nil)
          (list "Status" 15 nil)
          (list "Instance" 15 nil)))

(navigel-method flow navigel-entity-to-columns ((entity flow-pipeline))
  "Return the column descriptors to display a flow-pipeline in a tabulated list"
  (vector (flow--pipeline-name entity)
          (flow--pipeline-status-property-string entity)
          (number-to-string (flow--pipeline-instance entity))))

(navigel-method flow navigel-open ((_pipeline flow-pipeline) _callback)
  "Call CALLBACK with the most recent instances."
  (flow--cleanup-buffer "*flow-patches*")
  (flow--cleanup-buffer "*flow-jobs*")
  (flow--cleanup-buffer "*flow-instances*")
  (flow--cleanup-buffer "*flow-main*")
  (cl-call-next-method))

(navigel-method flow navigel-children ((pipeline flow-pipeline) callback)
  "Call CALLBACK with the most recent instances."
  (funcall callback (flow--pipeline-history pipeline)))

(navigel-method flow navigel-tablist-format ((_entity flow-pipeline))
  "Return a vector specifying columns to display pipelines's children."
  (vector (list "Instance" 15 nil)
          (list "Active" 10 nil)
          (list "Recent stage" 20 nil)
          (list "Scheduled" 20 nil)))

(navigel-method flow navigel-entity-to-columns ((entity flow-instance))
  "Return the column descriptors to display a flow-instance in a tabulated list"
  (let ((recent-stage (flow--stage-most-recent entity)))
    (vector  (number-to-string (flow--instance-number entity))
             (if (flow-running-p entity) "*" "")
             (if recent-stage (flow-status entity) "")
             (propertize (flow--instance-scheduled-time entity) 'face 'font-lock-doc-face))))

(navigel-method flow navigel-open ((entity flow-instance) _target)
  "Open a buffer displaying ENTITY."
  (let ((marked-instances (navigel-marked-entities))
        (delta)
        (patches)
        (jobs))
    (if marked-instances
        (setq delta (flow-delta-create marked-instances))
      (setq delta (flow-delta-create entity)))
    ;; It seems like multiple pipelines can use the same git material. The
    ;; consequence is that a lot of duplicated patches will be returned by
    ;; `flow-patches'. To mitigate this use `seq-uniq' to filter out the
    ;; duplicates.
    (setq patches (flow-patches delta))
    (setq patches (vconcat (seq-uniq patches)))
    (setq jobs (flow-jobs delta))
    (if (not (seq-empty-p jobs))
        (progn
          (flow-open-patches patches (seq-elt patches 0))
          (flow--highlight)
          (flow-open-jobs jobs (seq-elt jobs 0))
          (flow--highlight)
          (call-interactively #'tablist-find-entry))
      (flow--cleanup-buffer "*flow-jobs*")
      (flow-open-patches patches (seq-elt patches 0))
      (flow--highlight)
      (call-interactively #'tablist-find-entry))))


;;;; Patches

(navigel-method flow-patches navigel-entity-buffer (_entity)
  "Return a string representing ENTITY in the buffer's name."
  (format "*flow-patches*"))

(defun flow-open-patches (entity target)
  "Open a buffer displaying ENTITY and goto TARGET."
  (let ((navigel-app 'flow-patches))
    (navigel-open entity target)))

(navigel-method flow-patches navigel-children (entity callback)
  "Call CALLBACK with the monitored pipelines as input."
  (funcall callback entity))

(navigel-method flow-patches navigel-tablist-format (_entity)
  "Return a vector specifying columns to display entity's children."
  (vector (list "Subject" 52 nil)
          (list "Repository" 15 nil)
          (list "Instance" 15 nil)))

(navigel-method flow-patches navigel-entity-to-columns ((entity flow-patch))
  "Return the column descriptors to display a flow-patch in a tabulated list"
  (vector (flow--patch-subject entity)
          (flow--patch-repository entity)
          (number-to-string (flow--patch-instance entity))))

(navigel-method flow-patches navigel-open ((entity flow-patch) _target)
  "Open a buffer displaying ENTITY."
  (flow-open-commit-message entity))


;;;; Jobs

(navigel-method flow-jobs navigel-entity-buffer (_entity)
  "Return a string representing ENTITY in the buffer's name."
  (format "*flow-jobs*"))

(defun flow-open-jobs (entity target)
  "Open a buffer displaying ENTITY and goto TARGET."
  (let ((navigel-app 'flow-jobs))
    (navigel-open entity target)))

(navigel-method flow-jobs navigel-open (_entity _target)
  "Open jobs or clean up the buffer if there aren't any."
  (cl-call-next-method))

(navigel-method flow-jobs navigel-children (entity callback)
  "Call CALLBACK with the monitored pipelines as input."
  (funcall callback entity))

(navigel-method flow-jobs navigel-tablist-format (_entity)
  "Return a vector specifying columns to display entity's children."
  (vector (list "Job" 40 nil)
          (list "Status" 10 nil)
          (list "Severity" 10 nil)
          (list "Stage" 10 nil)
          (list "Instance" 10 nil)))

(navigel-method flow-jobs navigel-entity-to-columns ((entity flow-job))
  "Return the column descriptors to display a flow-patch in a tabulated list"
  (vector (flow--job-name entity)
          (flow--job-result entity)
          (symbol-name (flow-severity entity))
          (flow--job-stage entity)
          (number-to-string (flow--job-instance entity))))

(navigel-method flow-jobs navigel-open ((entity flow-job) _target)
  "Open a buffer displaying ENTITY."
  (flow-open-job entity))

;; Higlights
(defun flow--highlight ()
  "Highlight current instance."
  (save-excursion
    (let ((inhibit-read-only t))
      (put-text-property (line-beginning-position) (line-end-position)
                         'face 'region))))

;;; `navigel' major-mode configuration

(navigel-method flow navigel-entity-tablist-mode (_entity)
  (flow-pipeline-mode))

(navigel-method flow navigel-entity-tablist-mode ((_entity flow-pipeline))
  (flow-instance-mode))

(navigel-method flow-patches navigel-entity-tablist-mode (_entity)
  (flow-patch-mode))

(navigel-method flow-jobs navigel-entity-tablist-mode (_entity)
  (flow-job-mode))

(provide 'flow-ui)

;;; flow-ui.el ends here
