;;; flow.el --- Investigate flow in GoCD from Emacs-*- lexical-binding: t; -*-

;; Copyright (C) 2019-2020 Niklas Carlsson, David Friberg, Jonathan Karlsson

;; Authors: Niklas Carlsson <niklas.carlsson@posteo.net>,
;;          David Friberg <david.n.friberg@gmail.com>,
;;          Jonathan Karlsson <karlsjona@gmail.com>
;; URL: https://gitlab.com/niklascarlsson/gocd/gocd.el
;; Version: 0.1
;; Package-Requires: ((emacs "27.1"))
;; Keywords: flow productivity gocd gerrit

;; This file is not part of GNU Emacs.

;;; Commentary:

;; The `flow' package provides a unified interface to monitor and investigate
;; flow. It combines information from gocd and gerrit which makes it possible
;; to:
;; - view all jobs that failed in gocd
;; - view all patches that were introduced in a build instance(s)
;; - view commit messages
;; - view patch diffs
;; - view console outputs from jobs run in gocd
;; - capture notes

;;;; Installation

;;;;; Manual

;; Install these required packages:

;; + navigel
;; + xclip

;; Then put this file in your load-path, and put this in your init
;; file:

;; (require 'flow)

;;;; Usage

;; Run one of these commands:

;; `flow': Opens all the flow windows.

;;;; Tips

;; + You can customize settings in the `flow' group.

;;;; Credits

;; This package would not have been possible without the following
;; packages: navigel[1], which helped us create nice tabulated-list views.
;;
;;  [1] https://github.com/DamienCassou/navigel/navigel.el

;;; License:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

;;;; Requirements

(require 'flow-core)
(require 'flow-ui)
(require 'xclip)
(require 'subr-x)
(require 'compile)
(require 'cl-extra)

;;;; Functions

(cl-defgeneric flow-capture ()
  "Capture selection or entity.")

(cl-defmethod flow-capture (&context (major-mode flow-compile-mode))
  "Capture the entity in MAJOR-MODE."
  (let ((text)
        (region-text))
    (setq text (format "The `%s` is failing in pipeline `%s`, instance `%s`.\n %s\n\n"
                       (flow--job-name flow-current)
                       (flow--job-pipeline flow-current)
                       (flow--job-instance flow-current)
                       (flow-url flow-current)))
    (when mark-active
      (setq region-text (buffer-substring-no-properties (mark) (point)))
      (setq text (concat text (format "The following error(s) occurs:\n```\n%s```\n\n" region-text))))
    text))

(cl-defmethod flow-capture (&context (major-mode flow-commit-message-mode))
  "Capture the entity in MAJOR-MODE."
  (let ((text)
        (author (format "@%s" (replace-regexp-in-string "\s" "_" (downcase (flow--patch-author flow-current))))))
    (setq text (format "Ping %s, your patch `%s` could be a reason %s.\n\n"
                       author
                       (flow--patch-subject flow-current)
                       (flow-url flow-current)))
    text))

(cl-defmethod flow-capture (&context (major-mode flow-diff-mode))
  "Capture the entity in MAJOR-MODE."
  (let ((text)
        (inhibit-message t)
        (author (format "@%s" (replace-regexp-in-string "\s" "_" (downcase (flow--patch-author flow-current))))))
    (setq text (format "Ping %s, your patch `%s` could be a reason %s.\n\n"
                       author
                       (flow--patch-subject flow-current)
                       (flow-url flow-current)))
    (when mark-active
      (let ((region-text (buffer-substring-no-properties (mark) (point)))
            (file-name (substring-no-properties (seq-elt (diff-hunk-file-names) 0))))
        (setq text (concat text (format "The change(s) in file %s look suspicious.\n```\n%s```\n" file-name region-text)))))
    (unless mark-active
      (save-excursion
        (diff-beginning-of-hunk)
        (call-interactively #'set-mark-command)
        (diff-end-of-hunk)
        (let ((region-text (buffer-substring-no-properties (mark) (point)))
              (file-name (substring-no-properties (seq-elt (diff-hunk-file-names) 0))))
          (setq text (concat text (format "The change(s) in file %s look suspicious.\n```\n%s```\n" file-name region-text))))
        (deactivate-mark)))
    (format "%s\n" text)))

(cl-defmethod flow-capture (&context (major-mode (derived-mode tablist-mode)))
  "Capture the entity in MAJOR-MODE."
  (let ((entity (or (navigel-marked-entities) (navigel-entity-at-point))))
    (pcase entity
      ((pred listp) (string-join (mapcar #'flow-capture-entity entity)))
      (_ (flow-capture-entity entity)))))

(defun flow-insert-note (text)
  "Insert TEXT in the dedicated note buffer."
  (let ((buffer (or (get-buffer "*flow-notes*")
                    (generate-new-buffer "*flow-notes*"))))
    (with-current-buffer buffer
      (goto-char (point-max))
      (insert text))))

(cl-defgeneric flow-capture-entity (entity)
  "Capture ENTITY.")

(cl-defmethod flow-capture-entity ((job flow-job))
  "Capture JOB entity."
  (let ((name (flow--job-name job))
        (url (flow-url job)))
    ;; (format "job %s, %s\n" name url)
    (format "<%s|%s>" url name)))

(cl-defmethod flow-capture-entity ((patch flow-patch))
  "Capture PATCH entity."
  (let* ((author (format "@%s" (replace-regexp-in-string "\s" "_" (downcase (flow--patch-author patch)))))
         (url (flow-url patch)))
    (format "patch %s %s\n" author url)))

(cl-defgeneric flow-url (entity)
  "Return an url for ENTITY.")

(cl-defmethod flow-url ((pipeline flow-pipeline))
  "Return an url for PIPELINE."
  (let* ((pipeline-name (flow--pipeline-name pipeline)))
    (format "%s/go/tab/pipeline/history/%s" flow-gocd-url pipeline-name)))

(cl-defmethod flow-url ((instance flow-instance))
  "Return an url for INSTANCE."
  (let* ((pipeline (flow--instance-pipeline instance))
         (number (flow--instance-number instance))
         (stage (flow--stage-most-recent instance))
         (stage-name (flow--stage-name stage))
         (counter (flow--stage-counter stage)))
    (format "%s/go/pipelines/%s/%s/%s/%s" flow-gocd-url pipeline number stage-name counter)))

(cl-defmethod flow-url ((job flow-job))
  "Return an url for JOB."
  (let* ((pipeline (flow--job-pipeline job))
         (instance (flow--job-instance job))
         (stage (flow--job-stage job))
         (name (flow--job-name job))
         (counter (flow--job-counter job)))
    (format "%s/go/tab/%s/detail/%s/%s/%s/%s/%s" flow-gocd-url stage pipeline instance stage counter name)))

(cl-defmethod flow-url ((patch flow-patch))
  "Return an url for PATCH."
  (let* ((repository (flow--patch-repository patch))
         (number (flow--patch-number patch)))
    (format "https://%s/c/%s/+/%d" flow-gerrit-url repository number)))

(cl-defgeneric flow-content (entity)
  "Return the content of ENTITY.")

(cl-defmethod flow-content ((patch flow-patch))
  "Return the content of PATCH."
  (let* ((repository (flow--patch-repository patch))
         (remote-url (apply #'concat `("ssh://"
                                       ,flow-gerrit-user
                                       "@" ,flow-gerrit-url ":"
                                       ,flow-gerrit-port "/" ,repository))))
    (apply #'call-process `("git" nil nil nil
                            "fetch"
                            ,remote-url
                            ,(flow--patch-ref patch)))
    (apply #'call-process `("git" nil t nil
                            "diff" "FETCH_HEAD~1..FETCH_HEAD"))))

(defun flow-open-job (&optional job)
  "Open the console output of JOB in other window."
  (interactive)
  (let* ((job (or job (navigel-entity-at-point)))
         (url (format "%s/go/files/%s/%d/%s/%s/%s/cruise-output/console.log"
                      flow-gocd-url
                      (flow--job-pipeline job)
                      (flow--job-instance job)
                      (flow--job-stage job)
                      (flow--job-counter job)
                      (flow--job-name job)))
         (header "Content-Type: application/json")
         (buffer (or (get-buffer "*flow-main*")
                     (generate-new-buffer "*flow-main*")))
         (inhibit-read-only t))
    (with-current-buffer buffer
      (erase-buffer)
      (unless (eq major-mode 'flow-compile-mode)
        (flow-compile-mode))
      (compilation-minor-mode)
      (setq flow-current job)
      (setq-local compilation-skip-threshold 2)
      (flow-query-gocd url header))
    (select-window (get-buffer-window buffer))
    (goto-char (point-min))
    (compilation-next-error 1)))

(defun flow-open-patch (&optional patch)
  "Open the diff content of the PATCH in other window."
  (interactive)
  (let ((patch (or patch (navigel-entity-at-point)))
        (buffer (or (get-buffer "*flow-main*")
                    (generate-new-buffer "*flow-main*")))
        (inhibit-read-only t))
    (with-current-buffer buffer
      (erase-buffer)
      (flow-content patch)
      (flow-diff-mode)
      (setq flow-current patch)
      (goto-char (point-min)))
    (select-window (get-buffer-window buffer))))

(defun flow-open-commit-message (&optional patch)
  "Show the commit message of the PATCH."
  (interactive)
  (let ((patch (or patch (navigel-entity-at-point)))
        (buffer (or (get-buffer "*flow-main*")
                    (generate-new-buffer "*flow-main*")))
        (inhibit-read-only t))
    (with-current-buffer buffer
      (erase-buffer)
      (insert (flow--patch-commit-message patch))
      (flow-commit-message-mode)
      (setq flow-current patch))
    (select-window (get-buffer-window buffer))))

(defun flow-next (buffer func &optional nocall)
  "Select the next entity in BUFFER and call FUNC depending on NOCALL."
  (save-selected-window
    (select-window (get-buffer-window buffer))
    (let ((current-line (line-number-at-pos))
          (max-line (count-lines (point-min) (point-max))))
      (when (< current-line max-line)
        (message "Debug info: %d %d" current-line max-line)
        (navigel-refresh)
        (call-interactively #'tablist-next-line)
        (flow--highlight)
        (unless nocall
          (call-interactively func))))))

(defun flow-prev (buffer func &optional nocall)
  "Select the prev entity in BUFFER and call FUNC depending on NOCALL."
  (save-selected-window
    (select-window (get-buffer-window buffer))
    (let ((current-line (line-number-at-pos)))
      (when (> current-line 1)
        (navigel-refresh)
        (call-interactively #'tablist-previous-line)
        (flow--highlight)
        (unless nocall
          (call-interactively func))))))

(cl-defmethod flow-failed-p ((instance flow-instance))
  "Return t if the INSTANCE has failed."
  (seq-some #'flow-failed-p (flow--instance-stages instance)))

(cl-defmethod flow-failed-p ((stage flow-stage))
  "Return STAGE if any job failed."
  (let ((result (flow--stage-result stage)))
    (when result
      (when (or (string-match-p "Failed" result)
                (string-match-p "Unknown" result))
        (when (seq-some #'flow-failed-p (flow--stage-jobs stage))
          stage)))))

(cl-defmethod flow-failed-p ((job flow-job))
  "Return JOB if it failed."
  (when (string-match-p "Failed" (flow--job-result job))
    job))

(cl-defmethod flow-failed-jobs ((instance flow-instance))
  "Return a vector of failed jobs from INSTANCE."
  (let ((jobs)
        (failed-jobs)
        (stages (flow--instance-stages instance)))
    (when (flow-failed-p instance)
      (seq-do (lambda (stage)
                (setq jobs (flow-failed-jobs stage))
                (when jobs
                  (setq failed-jobs (cl-concatenate 'vector failed-jobs jobs)))
                ) stages)
      failed-jobs)))

(cl-defmethod flow-failed-jobs ((delta flow-delta))
  "Create a vector of patches from a DELTA."
  (let ((instances (flow-instances delta))
        (failed-jobs))
    (seq-do (lambda (instance)
              (setq failed-jobs (cl-concatenate 'vector failed-jobs (flow-failed-jobs instance)))) instances)
    failed-jobs))

(cl-defmethod flow-failed-jobs ((instance flow-instance))
  "Return a vector of failed jobs from INSTANCE."
  (let ((jobs)
        (failed-jobs)
        (stages (flow--instance-stages instance)))
    (when (flow-failed-p instance)
      (seq-do (lambda (stage)
                (setq jobs (flow-failed-jobs stage))
                (when jobs
                  (setq failed-jobs (cl-concatenate 'vector failed-jobs jobs)))
                ) stages)
      failed-jobs)))

(cl-defmethod flow-failed-jobs ((stage flow-stage))
  "Return a vector of failed jobs from STAGE."
  (when (flow-failed-p stage)
      (seq-filter #'flow-failed-p (flow--stage-jobs stage))))


;;;; Commands

;;;###autoload
(defun flow ()
  "Open the flow workspace."
  (interactive)
  (let ((buffers '("*flow-main*" "*flow-pipelines*" "*flow-instances*" "*flow-jobs*" "*flow-patches*"))
        (main-window (selected-window)))
    (mapc (lambda (buffer) (let ((inhibit-read-only t))
                        (when (get-buffer buffer)
                          (with-current-buffer buffer
                            (erase-buffer))))) buffers)

    (switch-to-buffer (or (get-buffer "*flow-main*")
                          (generate-new-buffer "*flow-main*")))
    (save-selected-window
      ;; piplines
      (select-window (split-window (selected-window) -15 'below))
      (switch-to-buffer (or (get-buffer "*flow-pipelines*")
                            (generate-new-buffer "*flow-pipelines*")) nil 0)
      (flow-pipeline-mode)
      ;; instances
      (select-window (split-window (selected-window) nil 'right))
      (switch-to-buffer (or (get-buffer "*flow-instances*")
                            (generate-new-buffer "*flow-instances*")) nil 0)
      (flow-instance-mode)
      ;; patches
      (select-window (split-window main-window -100 'right))
      (switch-to-buffer (or (get-buffer "*flow-patches*")
                            (generate-new-buffer "*flow-patches*")) nil 0)
      ;; jobs
      (select-window (split-window (selected-window) nil 'below))
      (switch-to-buffer (or (get-buffer "*flow-jobs*")
                            (generate-new-buffer "*flow-jobs*")) nil 0))
    (switch-to-buffer (or (get-buffer "*flow-main*")
                          (generate-new-buffer "*flow-main*")))
    (read-only-mode)
    (flow-list-pipelines)))

;;;###autoload
(defun flow-capture-note ()
  "Capture a note at point."
  (interactive)
  (flow-insert-note (flow-capture)))

;;;###autoload
(defun flow-next-error ()
  "Move point to the next error in the buffer."
  (interactive)
  (compilation-next-error 1))

;;;###autoload
(defun flow-prev-error ()
  "Move point to the previous error in the buffer."
  (interactive)
  (compilation-previous-error 1))

;;;###autoload
(defun flow-copy-entity ()
  "Copy entity's url into the clipboard."
  (interactive)
  (xclip-set-selection 'clipboard (flow-url (navigel-entity-at-point))))

;;;###autoload
(defun flow-browse-entity ()
  "Browse entity at point."
  (interactive)
  (browse-url (flow-url (navigel-entity-at-point))))

;;;###autoload
(defun flow-next-job ()
  "Select the next job."
  (interactive)
  (let ((buffer "*flow-jobs*"))
    (if (string= (buffer-name) buffer)
        (flow-next  buffer #'flow-open-job t)
      (flow-next  buffer #'flow-open-job))))

;;;###autoload
(defun flow-prev-job ()
  "Select the previous job."
  (interactive)
  (let ((buffer "*flow-jobs*"))
    (if (string= (buffer-name) buffer)
        (flow-prev  buffer #'flow-open-job t)
      (flow-prev  buffer #'flow-open-job))))

;;;###autoload
(defun flow-next-patch ()
  "Select the next patch."
  (interactive)
  (flow-next "*flow-patches*" #'flow-open-patch))

;;;###autoload
(defun flow-prev-patch ()
  "Select the previous patch."
  (interactive)
  (flow-prev "*flow-patches*" #'flow-open-patch))

;;;###autoload
(defun flow-next-commit-message ()
  "Select the next commit message."
  (interactive)
  (flow-next "*flow-patches*" #'flow-open-commit-message))

;;;###autoload
(defun flow-prev-commit-message ()
  "Select the previous commit message."
  (interactive)
  (flow-prev "*flow-patches*" #'flow-open-commit-message))

;;;###autoload
(defun flow-next-pipeline ()
  "Select the next pipeline."
  (interactive)
  (save-selected-window
    (call-interactively #'tablist-next-line)
    (call-interactively #'tablist-find-entry)))

;;;###autoload
(defun flow-prev-pipeline ()
  "Select the previous pipeline."
  (interactive)
  (save-selected-window
    (call-interactively #'tablist-previous-line)
    (call-interactively #'tablist-find-entry)))

;;;###autoload
(defun flow-switch-to-job ()
  "Switch to viewing job output."
  (interactive)
  (save-selected-window
    (select-window (get-buffer-window "*flow-jobs*"))
    (call-interactively #'tablist-find-entry (navigel-entity-at-point))))

;;;###autoload
(defun flow-switch-to-commit-message ()
  "Switch to viewing commit messages."
  (interactive)
  (save-selected-window
    (select-window (get-buffer-window "*flow-patches*"))
    (flow-open-commit-message (navigel-entity-at-point))))

;;;###autoload
(defun flow-switch-to-diff ()
  "Switch to viewing patch diff."
  (interactive)
  (save-selected-window
    (select-window (get-buffer-window "*flow-patches*"))
    (call-interactively #'flow-open-patch)))

;;;###autoload
(defun flow-open-note ()
  "Open the note buffer."
  (interactive)
  (let ((note-buffer (or (get-buffer "*flow-notes*")
                         (generate-new-buffer "*flow-notes*"))))
    (if-let (win (get-buffer-window note-buffer))
        (unless (eq (selected-window) win)
          (select-window win)
          (goto-char (point-max)))
      (with-current-buffer note-buffer
        (unless (eq major-mode 'flow-note-mode) (flow-note-mode)))
      (save-selected-window
        (pop-to-buffer note-buffer)))))

(provide 'flow)

;;; flow.el ends here
