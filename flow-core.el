;;; flow-core.el --- Provide code to be reused by flow modes -*- lexical-binding: t; -*-

;; Copyright (C) 2019-2020 Niklas Carlsson, David Friberg, Jonathan Karlsson

;; Authors: Niklas Carlsson <niklas.carlsson@posteo.net>,
;;          David Friberg <david.n.friberg@gmail.com>,
;;          Jonathan Karlsson <karlsjona@gmail.com>
;; URL: https://gitlab.com/niklascarlsson/gocd/gocd.el
;; Version: 0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This file provides some common ground for all flow user-interfaces.

;;; Code:

;;;; Requirements
(require 'cl-lib)
(require 'hide-mode-line)
(require 'so-long)
(require 'diff-mode)
(require 'so-long)
(require 'navigel)

;;;; Customization

(defcustom flow-gocd-url "https://gocd.example.com"
  "The URL to the REST Api access point."
  :group 'flow
  :type 'string)

(defcustom flow-gocd-email nil
  "The user email adress."
  :group 'flow
  :type 'string)

(defcustom flow-gocd-secret nil
  "The secret to use for authentication."
  :group 'flow
  :type 'string)

(defcustom flow-gerrit-user nil
  "The username for Gerrit."
  :group 'flow
  :type 'string)

(defcustom flow-gerrit-port nil
  "The port to use to communicate with Gerrit."
  :group 'flow
  :type 'string)

(defcustom flow-gerrit-url nil
  "The url to use to communicate with Gerrit."
  :group 'flow
  :type 'string)

(defcustom flow-min-pipeline-instances 20
  "Number of minimum pipeline instances to show."
  :group 'flow
  :type 'number)

(defcustom flow-monitor-pipelines '("pipeline1"
                                    "pipeline2")
  "A list of pipelines to monitor."
  :group 'flow
  :type 'list)

(defcustom flow-stage-severity-error-alist '(("pipeline1" . ("stage1" "stage2"))
                                             ("pipeline2" . ("stage1")))
  "An alist of pipeline stages to label error if they fail."
  :group 'flow
  :type 'alist)

(defcustom flow-stage-severity-warn-alist '(("pipeline2" . ("stage2")))
  "An alist of pipeline stages to label warn if they fail."
  :group 'flow
  :type 'alist)

(defvar-local flow-current nil
  "The flow content in the current buffer.")

;;;; Data structures

(cl-defstruct (flow-pipeline (:constructor flow--pipeline-create)
                             (:conc-name flow--pipeline-))
  (name nil :read-only t)
  (status nil :read-only t)
  (instance nil :read-only t)
  (history nil :read-only t))

(cl-defstruct (flow-instance (:constructor flow--instance-create)
                             (:conc-name flow--instance-))
  (number nil :read-only t)
  (pipeline nil :read-only t)
  (active-stage nil :read-only t)
  (failed-stage nil :read-only t)
  (stages nil :read-only t)
  (materials nil :read-only t))

(cl-defstruct (flow-delta (:constructor flow--delta-create)
                             (:conc-name flow--delta-))
  (pipeline nil :read-only t)
  (from nil :read-only t)
  (to nil :read-only t))

(cl-defstruct (flow-pipeline-material (:constructor flow--pipeline-material-create)
                                      (:conc-name flow--pipeline-material-))
  (changed nil :read-only t)
  (pipeline nil :read-only t)
  (instance nil :read-only t))

(cl-defstruct (flow-git-material (:constructor flow--git-material-create)
                                      (:conc-name flow--git-material-))
  (changed nil :read-only t)
  (revision nil :read-only t))

(cl-defstruct (flow-stage (:constructor flow--stage-create)
                        (:conc-name flow--stage-))
  (name nil :read-only t)
  (result nil :read-only t)
  (scheduled nil :read-only t)
  (scheduled_date nil :read-only t)
  (jobs nil :read-only t)
  (counter nil :read-only t)
  (instance nil :read-only t)
  (pipeline nil :read-only t))

(cl-defstruct (flow-job (:constructor flow--job-create)
                        (:conc-name flow--job-))
  (name nil :read-only t)
  (result nil :read-only t)
  (stage nil :read-only t)
  (scheduled nil :read-only t)
  (counter nil :read-only t)
  (instance nil :read-only t)
  (pipeline nil :read-only t))

(cl-defstruct (flow-patch (:constructor flow--patch-create)
                             (:conc-name flow--patch-))
  (subject nil :read-only t)
  (author nil :read-only t)
  (number nil :read-only t)
  (commit-message nil :read-only t)
  (ref nil :read-only t)
  (repository nil :read-only t)
  (pipeline nil)
  (instance nil))

;;;; Variables

(defvar flow-pipelines-data nil
  "Hold the pipeline objects from the last GOCD query.")

;;;; Functions
(defun flow-stage-result (instance stage-name)
  "Return the result for INSTANCE's STAGE-NAME.
Only final results are returned, not those that are unknown."
  (seq-some (lambda (stage)
              (let ((result (flow--stage-result stage))
                    (name (flow--stage-name stage)))
                (when (and (string= name stage-name)
                           (not (flow-running-p stage)))
                  result))) (flow--instance-stages instance)))

(defun flow-instances-result (instances stage-name)
  "Return the result of the first instance in INSTANCES that as a result for STAGE-NAME."
  (seq-some (lambda (instance)
              (flow-stage-result instance stage-name)) instances))

(defun flow-pipeline-stages-status (pipeline)
  "Return an alist with status for the different stages in PIPELINE."
  (let* ((instances (flow--pipeline-history pipeline))
         (first-instance (seq-elt instances 0))
         (stages (mapcar (lambda (stage)
                           (flow--stage-name stage)) (flow--instance-stages first-instance))))
    (mapcar (lambda (stage) `(,stage . ,(flow-instances-result instances stage)))
            stages)))

(defun flow-pipeline-status (pipeline)
  "Return the status for PIPELINE."
  (let ((error-stages (cdr (assoc (flow--pipeline-name pipeline) flow-stage-severity-error-alist)))
        (warn-stages (cdr (assoc (flow--pipeline-name pipeline) flow-stage-severity-warn-alist)))
        (stages-status (flow-pipeline-stages-status pipeline)))
    (cond ((seq-some (lambda (stage)
                       (when-let ((value (assoc stage stages-status)))
                         (string= "Failed" (cdr value)))
                       ) error-stages) 'error)
          ((seq-some (lambda (stage)
                       (when-let ((value (assoc stage stages-status)))
                         (string= "Failed" (cdr value)))
                       ) warn-stages) 'warn)
          (t 'success))))

(cl-defgeneric flow-failed-p (entity)
  "Return t if ENTITY failed.")

(cl-defgeneric flow-failed-jobs (entity)
  "Return a vector of failed jobs from ENTITY.")

(defun flow-query-gocd (url header)
  "Query URL with HEADER and return the response."
  (let ((user (concat flow-gocd-email ":" flow-gocd-secret)))
    (apply #'call-process `("curl" nil t nil
                            "--silent"
                            "--header" ,header
                            "--user" ,user
                            "--request" "GET" ,url))))

(defun flow-query-gerrit (sha)
  "Query Gerrit for patch-set with SHA and return json object."
  (let* ((query-args (format "commit:%s" sha)))
    (with-temp-buffer
      (apply #'call-process
             `("ssh" nil t nil
               "-p" ,flow-gerrit-port
               ,flow-gerrit-url
               "gerrit" "query"
               "--format" "JSON"
               "--current-patch-set"
               ,query-args))
      (goto-char (point-min))
      (json-parse-buffer :array-type 'array
                         :object-type 'alist
                         :null-object nil
                         :false-object nil))))

(defun flow-pipelines ()
  "Return a list of updated `flow-pipeline' objects."
  (let ((pipelines (cl-map 'vector #'flow-pipeline flow-monitor-pipelines)))
    (setq flow-pipelines-data pipelines)
    flow-pipelines-data))

(defun flow-pipeline (pipeline-name)
  "Return a flow-pipeline representation of PIPELINE-NAME."
  (let ((raw-instances (flow--query-pipeline-history pipeline-name flow-min-pipeline-instances))
        (instances))
    (setq instances (cl-map 'vector #'flow-instance raw-instances))
    (flow--pipeline-create :name pipeline-name
                           :instance (flow--instance-number (flow--instance-most-recent instances))
                           :status "OK"
                           :history instances)))

(cl-defgeneric flow-instance ()
  "Return a flow-instance.")

(cl-defmethod flow-instance ((raw-instance list))
  "Return a flow-instance created from a RAW-INSTANCE."
  (let* ((stages (flow-stages raw-instance))
         (materials (flow-materials raw-instance)))
    (let-alist raw-instance
      (flow--instance-create :number .counter
                             :pipeline .name
                             :active-stage (flow--instance-active-p stages)
                             :failed-stage (flow--instance-stage-failure-p stages)
                             :stages stages
                             :materials materials))))

(cl-defmethod flow-instance ((material flow-pipeline-material))
  "Return an instance created from a MATERIAL.
TODO: Address the query made in this function."
  (let* ((pipeline (flow--pipeline-material-pipeline material))
         (instance (flow--pipeline-material-instance material))
         (url (format "%s/go/api/pipelines/%s/instance/%d"
                      flow-gocd-url
                      pipeline
                      instance))
         (header "Content-Type: application/json")
         (raw-material))
    (with-temp-buffer
      (erase-buffer)
      (flow-query-gocd url header)
      (goto-char (point-min))
      (setq raw-material (json-parse-buffer :array-type 'array
                                            :object-type 'alist
                                            :null-object nil
                                            :false-object nil)))
    (flow-instance raw-material)))

(cl-defgeneric flow-instances (entity)
  "Create a list of instances from ENTITY.")

(cl-defmethod flow-instances ((delta flow-delta))
  "Create a list of flow-instance objects from a DELTA."
  (let ((pipeline-name (flow--delta-pipeline delta))
        (from-instance (flow--delta-from delta))
        (to-instance (flow--delta-to delta))
        (pipeline))
    (setq pipeline (seq-some (lambda (pipeline)
                               (when (string= pipeline-name (flow--pipeline-name pipeline))
                                 pipeline)) flow-pipelines-data))
    (seq-filter (lambda (instance) (<= from-instance (flow--instance-number instance) to-instance))
                (flow--pipeline-history pipeline))))

(cl-defgeneric flow-delta-create (entity)
  "Create a flow-delta from ENTITY.")

(cl-defmethod flow-delta-create ((instances list))
  "From a list of INSTANCES generate a delta object."
  (flow--delta-create :pipeline (flow--instance-pipeline (seq-elt instances 0))
                      :from (seq-min (mapcar (lambda (instance) (flow--instance-number instance)) instances))
                      :to (seq-max (mapcar (lambda (instance) (flow--instance-number instance)) instances))))

(cl-defmethod flow-delta-create ((instance flow-instance))
  "From an INSTANCE generate a delta object."
  (flow--delta-create :pipeline (flow--instance-pipeline instance)
                      :from (flow--instance-number instance)
                      :to (flow--instance-number instance)))

(defun flow-material-create (material)
  "Create a flow-material from a raw MATERIAL."
  (let ((changed))
    (let-alist material
      (setq changed (eq .changed t))
      (if (string= .material.type "Pipeline")
          (vector (flow--pipeline-material-create :changed changed
                                                   :pipeline (flow--raw-material-pipeline material)
                                                   :instance (flow--raw-material-instance-number material)))
        (cl-map 'vector (lambda (git-material)
                          (flow--git-material-create :changed changed
                                                     :revision (let-alist git-material
                                                                 .revision))) .modifications)))))

(defun flow--severity-error-p (pipeline stage)
  "Return t if the PIPELINE's STAGE has severity error."
  (let ((error-stages (assoc pipeline flow-stage-severity-error-alist)))
    (cl-member stage error-stages :test #'string=)))

(cl-defgeneric flow-severity (entity)
  "Return the severity status for ENTITY.")

(cl-defmethod flow-severity ((instance flow-instance))
  "Return the severity status for INSTANCE."
  (let* ((stage (flow--stage-most-recent instance))
         (pipeline (flow--stage-pipeline stage))
         (stage-name (flow--stage-name stage)))
    (cond ((flow--severity-error-p pipeline stage-name) 'error)
          ((flow--severity-warn-p pipeline stage-name) 'warn)
          (t 'info))))

(cl-defmethod flow-severity ((stage flow-stage))
  "Return the severity status for STAGE."
  (let ((pipeline (flow--stage-pipeline stage))
        (stage-name (flow--stage-name stage)))
    (cond ((flow--severity-error-p pipeline stage-name) 'error)
          ((flow--severity-warn-p pipeline stage-name) 'warn)
          (t 'info))))

(cl-defmethod flow-severity ((job flow-job))
  "Return the severity status for JOB."
  (let ((pipeline (flow--job-pipeline job))
        (stage (flow--job-stage job)))
    (cond ((flow--severity-error-p pipeline stage) 'error)
          ((flow--severity-warn-p pipeline stage) 'warn)
          (t 'info))))

(cl-defgeneric flow-status (entity)
  "Return the status for ENTITY.")

(cl-defgeneric flow-status ((job flow-job))
  "Return the status for JOB."
  (if (flow-failed-p job)
      (flow--status-property (flow-severity job) "Failed")
    (flow--status-property 'success "Success")))

(cl-defgeneric flow-status ((instance flow-instance))
  "Return the status for INSTANCE."
  (let* ((stage (flow--stage-most-recent instance))
         (severity (flow-severity stage))
         (name (flow--stage-name stage))
         (running (flow-running-p stage))
         (failed (flow-failed-p stage)))
    (cond ((and running failed) (flow--status-property severity name))
          ((and running (not failed)) name)
          ((and (not running) failed) (flow--status-property severity name))
          ((and (not running) (not failed)) (flow--status-property 'success name)))))

(cl-defgeneric flow-running-p (entity)
  "Return t if ENTITY is running.")

(cl-defmethod flow-running-p ((job flow-job))
  "Return t if JOB is running."
  (string-match-p "Unknown" (flow--job-result job)))

(cl-defmethod flow-running-p ((stage flow-stage))
  "Return t if STAGE is running."
  (let ((result (flow--stage-result stage)))
    (when result (string-match-p "Unknown" result))))

(cl-defmethod flow-running-p ((instance flow-instance))
  "Return t if INSTANCE is running."
  (seq-some #'flow-running-p (flow--instance-stages instance)))

(cl-defgeneric flow-materials ()
  "Return a vector of flow-material.")

(cl-defmethod flow-materials ((raw-instance list))
  "Return a vector of materials created from a RAW-INSTANCE."
  (let ((material-revisions)
        (materials))
    (let-alist raw-instance
      (setq material-revisions .build_cause.material_revisions))
    (seq-do (lambda (material)
              (setq materials (cl-concatenate 'vector materials
                                           (flow-material-create material)))) material-revisions)
    materials))

(cl-defgeneric flow-stage (entity)
  "Return a flow-stage related to ENTITY.")

(cl-defmethod flow-stage ((instance flow-instance))
  "Return a flow-stage related to INSTANCE."
  (flow--stage-most-recent instance ))

(cl-defmethod flow-stage ((job flow-job))
  "Return a flow-stage object related to JOB."
  (let* ((pipeline-name (flow--job-pipeline job))
         (stage-name (flow--job-stage job))
         (instance-number (flow--job-instance job))
         (pipeline)
         (instance))
    (setq pipeline (seq-some (lambda (pipeline)
                               (when (string= (flow--pipeline-name pipeline) pipeline-name)
                                 pipeline)) flow-pipelines-data))
    (setq instance (seq-some (lambda (instance)
                               (when (= (flow--instance-number instance) instance-number)
                                 instance)) (flow--pipeline-history pipeline)))
    (seq-some (lambda (stage)
                (when (string= (flow--stage-name stage) stage-name)
                  stage)) (flow--instance-stages instance))))

(cl-defgeneric flow-stages (entity)
  "Return flow-stages related to ENTITY.")

(cl-defmethod flow-stages ((raw-instance list))
  "Return a vector of flow-stage created from a RAW-INSTANCE."
  (let-alist raw-instance
    (cl-map 'vector (lambda (stage)
                      (flow-stage-create `(, .name , .counter ,stage))) .stages)))

(defun flow-stage-create (input)
  "Create a flow-job object from INPUT."
  (pcase input
    (`(,pipeline ,instance ,stage)
     (let-alist stage
       (if .scheduled
           (progn
             (let ((jobs (flow-jobs `(,pipeline ,instance ,stage))))
               (flow--stage-create :name .name
                                   :result .result
                                   :scheduled .scheduled
                                   :scheduled_date (let-alist (seq-elt .jobs 0)
                                                     (string-to-number (substring (number-to-string .scheduled_date) 0 10)))
                                   :jobs jobs
                                   :counter .counter
                                   :instance instance
                                   :pipeline pipeline)))
         (flow--stage-create :name .name
                             :result .result
                             :instance instance
                             :pipeline pipeline))))))

(defun flow-job-create (stage pipeline instance job counter)
  "Create a flow-job object from STAGE, PIPELINE, INSTANCE, JOB and COUNTER."
  (let-alist job
    (flow--job-create :name .name
                      :result .result
                      :stage stage
                      :scheduled (string-to-number (substring (number-to-string .scheduled_date) 0 10))
                      :counter counter
                      :instance instance
                      :pipeline pipeline)))

(cl-defgeneric flow-jobs (entity)
  "Return flow-jobs related to ENTITY.")

(cl-defmethod flow-jobs ((input list))
  "Return flow-jobs for INPUT."
  (pcase-let ((`(,pipeline ,instance ,stage) input))
    (let ((jobs))
      (let-alist stage
        (seq-do (lambda (job)
                  (push (flow-job-create .name pipeline instance job .counter) jobs))
                .jobs))
      (vconcat jobs))))

(cl-defmethod flow-jobs ((instance flow-instance))
  "Return a list of failed jobs for INSTANCE."
  (let ((jobs (flow-failed-jobs instance)))
    jobs))

(cl-defmethod flow-jobs ((delta flow-delta))
  "Return a list of failed jobs for DELTA."
  (let ((instances (flow-instances delta))
        (jobs))
    (seq-do (lambda (instance)
              (setq jobs (cl-concatenate 'vector (flow-jobs instance) jobs))) instances)
    jobs))


(cl-defgeneric flow-patch-create (entity)
  "From an ENTITY create a flow-patch.")

(cl-defmethod flow-patch-create ((material flow-git-material))
  "From the MATERIAL create a patch.
Sometimes materials enter through some automatic CI user and the sha will not be
accessible in Gerrit. For those the function will return nil."
 (let ((patch-set (flow-query-gerrit (flow--git-material-revision material))))
    (let-alist patch-set
      (if (null .subject)
          nil
        (flow--patch-create :subject .subject
                            :author .currentPatchSet.author.name
                            :number .number
                            :commit-message .commitMessage
                            :ref .currentPatchSet.ref
                            :repository .project)))))

(cl-defgeneric flow-patches (entity)
  "Create a vector of patches from ENTITY.")

(cl-defmethod flow-patches ((instance flow-instance))
  "Create a vector of patches from an INSTANCE.
`flow--patches' creates the actual objects but they need to be updated with the
right pipeline and instance information."
  (let ((patches (flow--patches instance)))
    (setq patches (seq-remove #'null patches))
    (cl-map 'vector (lambda (patch) (setf (flow--patch-instance patch) (flow--instance-number instance))
                      (setf (flow--patch-pipeline patch) (flow--instance-pipeline instance))
                      patch) patches)))

(cl-defmethod flow-patches ((delta flow-delta))
  "Create a vector of patches from a DELTA."
  (let ((instances (flow-instances delta))
        (patches))
    (seq-do (lambda (instance)
              (setq patches (cl-concatenate 'vector (flow-patches instance) patches))) instances)
    patches))


;;;; Support functions

(defun flow--severity-warn-p (pipeline stage)
  "Return t if the PIPELINE's STAGE has severity warn."
(let ((warn-stages (assoc pipeline flow-stage-severity-warn-alist)))
    (cl-member stage warn-stages :test #'string=)))

(defun flow--status-property (status text)
  "Return a propertized TEXT condition on STATUS."
  (cond ((eq status 'error) (propertize text 'face 'error))
        ((eq status 'warn) (propertize text 'face 'warning))
        ((eq status 'info) (propertize text 'face 'font-lock-doc-face))
        ((eq status 'success) (propertize text 'face 'success))))

(defun flow--ui-stage-name (stage)
  "From STAGE return the name with added severity property."
  (let ((name (flow--stage-name stage))
        (severity (flow-severity stage)))
    (cond ((eq severity 'error) (propertize name 'face 'error))
          ((eq severity 'warn) (propertize name 'face 'warning))
          ((eq severity 'info) (propertize name 'face 'font-lock-doc-face)))))

(defun flow--cleanup-buffer (buffer)
  "Cleanup the BUFFER."
  (let ((inhibit-read-only t))
    (with-current-buffer (get-buffer buffer)
      (erase-buffer))))

(defun flow--instance-scheduled-time (instance)
  "Return the scheduled time as a string for INSTANCE."
  (let ((stage (flow--stage-most-recent instance)))
    (format-time-string "%Y-%m-%d %H:%M:%S" (flow--stage-scheduled_date stage))))

(defun flow--stage-most-recent (instance)
  "Return the most recently scheduled stage for INSTANCE."
  (let ((stages (flow--instance-stages instance)))
    (seq-some (lambda (stage) (when (flow--stage-scheduled stage) stage) )
              (seq-reverse stages))))

(defun flow--instance-most-recent (instances)
  "Return the most recently finished instance from INSTANCES."
  (seq-some (lambda (instance)
              (unless (flow--instance-active-stage instance) instance)) instances))

(defun flow--instance-active-p (stages)
  "Return the name of the active stage in STAGES if instance is active."
  (seq-some (lambda (stage)
              (when (and (flow--stage-scheduled stage)
                         (string-match-p "Unknown" (flow--stage-result stage)))
                (flow--stage-name stage))) stages))

(defun flow--instance-stage-failure-p (stages)
  "Return the name of a failing stage from STAGES."
  (seq-some (lambda (stage)
              (when (and (flow--stage-scheduled stage)
                     (string-match-p "Failed" (flow--stage-result stage)))
                (flow--stage-name stage))) stages))

(defun flow--merge-pipeline-histories (histories)
  "Takes a sequence of raw pipeline HISTORIES and merge them to one."
  (let ((pipelines))
    (seq-do (lambda (history)
              (let-alist history
                (setq pipelines (cl-concatenate 'vector .pipelines pipelines)))) histories)
    pipelines))


(defun flow--raw-material-pipeline (material)
  "From a raw MATERIAL extract the pipeline name."
  (let-alist material
    .material.description))

(defun flow--raw-material-instance-number (material)
  "From a raw MATERIAL extract the instance number."
  (let-alist material
    (let-alist (seq-elt .modifications 0)
      (save-match-data
        (string-match (concat (flow--raw-material-pipeline material) "/\\([-0-9]+\\)")
                      .revision)
        (string-to-number (match-string 1 .revision))))))

(defun flow--fetch-pipelines ()
  "Fetch the latest pipelines."
(let ((pipelines (cl-map 'vector #'flow-pipeline flow-monitor-pipelines)))
    (setq flow-pipelines-data pipelines)))

(defun flow--pipeline-status-property-string (pipeline)
  "Return a status property string for PIPELINE."
  (let ((status (flow-pipeline-status pipeline)))
    (cond ((eq status 'error) (propertize "Failed" 'face 'error))
          ((eq status 'warn) (propertize "Failed" 'face 'warning))
          ((eq status 'success) (propertize "Success" 'face 'success)))))

(cl-defgeneric flow--patches (entity)
  "Return all new git patches related to ENTITY.")

(cl-defmethod flow--patches ((material flow-pipeline-material))
  "Return a vector of patches from MATERIAL."
  (flow--patches (flow-instance material)))

(cl-defmethod flow--patches ((material flow-git-material))
  "Return a vector of patches from MATERIAL."
  (vector (flow-patch-create material)))

(cl-defmethod flow--patches ((instance flow-instance))
  "Return all new git patches for INSTANCE."
  (let ((materials (flow--new-material instance))
        (patches))
    (cl-dolist (material materials)
      (setq patches (cl-concatenate 'vector (flow--patches material) patches)))
    patches))

(cl-defmethod flow--patches ((delta flow-delta))
  "Return a vector of flow-patch for DELTA."
  (let ((instances (flow-instances delta))
        (patches))
    (seq-do (lambda (instance)
              (setq patches (cl-concatenate 'vector patches (flow--patches instance)))) instances)
    patches))

(defun flow--new-material (instance)
  "Return the new material from an INSTANCE."
  (let ((materials (flow--instance-materials instance)))
    (seq-filter (lambda (material) (flow--material-changed material)) materials)))

(cl-defgeneric flow--material-changed (entity)
  "Return t if ENTITY changed.")

(cl-defmethod flow--material-changed ((material flow-git-material))
  "Return t if the MATERIAL had status changed."
  (flow--git-material-changed material))

(cl-defmethod flow--material-changed ((material flow-pipeline-material))
  "Return t if the MATERIAL had status changed."
  (flow--pipeline-material-changed material))

(defun flow--query-pipeline-history (pipeline-name size)
  "Return a vector of minimum SIZE raw instances for PIPELINE-NAME."
  (let* ((page-size 10)
         (offset)
         (number-of-requests (fceiling (/ (float size) page-size)))
         (url (format "%s/go/api/pipelines/%s/history/"
                      flow-gocd-url
                      pipeline-name))
         (json-objects)
         (header "Content-Type: application/json"))
    (dotimes (i number-of-requests)
      (with-temp-buffer
        (erase-buffer)
        (setq offset (* i page-size))
        (flow-query-gocd (format "%s%d" url offset) header)
        (goto-char (point-min))
        (setq json-objects (cl-concatenate 'vector (vector (json-parse-buffer :array-type 'array
                                                                           :object-type 'alist
                                                                           :null-object nil
                                                                           :false-object nil)) json-objects))))
    (flow--merge-pipeline-histories json-objects)))

;;;; Major Modes
(defcustom flow-pipeline-hook nil
  "Mode hook for Flow Pipeline mode, run after the mode was turned on."
  :type 'hook
  :group 'flow)

(defcustom flow-instance-hook nil
  "Mode hook for Flow Instance mode, run after the mode was turned on."
  :type 'hook
  :group 'flow)

(defcustom flow-patch-hook nil
  "Mode hook for Flow Patch mode, run after the mode was turned on."
  :type 'hook
  :group 'flow)

(defcustom flow-job-hook nil
  "Mode hook for Flow Job mode, run after the mode was turned on."
  :type 'hook
  :group 'flow)

;;;###autoload
(define-derived-mode flow-pipeline-mode navigel-tablist-mode "Flow Pipeline"
  "Major mode to display the current pipelines."
  (add-hook 'flow-pipeline-hook #'hide-mode-line-mode nil t)
  (run-hooks 'flow-pipeline-hook))

(defvar flow-pipeline-mode-map
  (let ((map (make-sparse-keymap)))
    (set-keymap-parent map navigel-tablist-mode-map)
    map)
  "Keybindings for `flow-pipeline-mode'.")

;;;###autoload
(define-derived-mode flow-instance-mode navigel-tablist-mode "Flow Instance"
  "Major mode to display the instances."
  (add-hook 'flow-instance-hook #'hide-mode-line-mode nil t)
  (run-hooks 'flow-instance-hook))

(defvar flow-instance-mode-map
  (let ((map (make-sparse-keymap)))
    (set-keymap-parent map navigel-tablist-mode-map)
    map)
  "Keybindings for `flow-instance-mode'.")

;;;###autoload
(define-derived-mode flow-patch-mode navigel-tablist-mode "Flow Patch"
  "Major mode to display the current patches."
  ;; TODO: Investigate why it's not activating flow--highlight
  (add-hook 'flow-patch-hook #'hide-mode-line-mode nil t)
  (run-hooks 'flow-patch-hook))

(defvar flow-patch-mode-map
  (let ((map (make-sparse-keymap)))
    (set-keymap-parent map navigel-tablist-mode-map)
    map)
  "Keybindings for `flow-patch-mode'.")

;;;###autoload
(define-derived-mode flow-job-mode navigel-tablist-mode "Flow Job"
  "Major mode to display the current jobs."
  (add-hook 'flow-job-hook #'hide-mode-line-mode nil t)
  (run-hooks 'flow-job-hook))

(defvar flow-job-mode-map
  (let ((map (make-sparse-keymap)))
    (set-keymap-parent map navigel-tablist-mode-map)
    map)
  "Keybindings for `flow-job-mode'.")

;;;###autoload
(define-derived-mode flow-diff-mode diff-mode "Flow Diff"
  "Major mode to display a patch diff.")

(defvar flow-diff-mode-map
  (let ((map (make-sparse-keymap)))
    (set-keymap-parent map diff-mode-map)
    map)
  "Keybindings for `flow-diff-mode'.")

;;;###autoload
(define-derived-mode flow-compile-mode so-long-mode "Flow Compile"
  "Major mode to display a compilation output.")

(defvar flow-compile-mode-map
  (let ((map (make-sparse-keymap)))
    (set-keymap-parent map so-long-mode-map)
    map)
  "Keybindings for `flow-compile-mode'.")

;;;###autoload
(define-derived-mode flow-commit-message-mode text-mode "Flow Commit Message"
  "Major mode to display a commit message.")

(defvar flow-commit-message-mode-map
  (let ((map (make-sparse-keymap)))
    (set-keymap-parent map text-mode-map)
    map)
  "Keybindings for `flow-commit-message-mode'.")

;;;###autoload
(define-derived-mode flow-note-mode text-mode "Flow Note"
  "Major mode for flow note taking.")

(defvar flow-note-mode-map
  (let ((map (make-sparse-keymap)))
    (set-keymap-parent map text-mode-map)
    map)
  "Keybindings for `flow-note-mode'.")

(provide 'flow-core)

;;; flow-core.el ends here
