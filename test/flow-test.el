;;  -*- lexical-binding: t; -*-

;;; Code:

;;;; Requirements

(require 'ert)
(require 'flow-core)
(require 'flow)

(ert-deftest flow-test-delta ()
  (let* ((instance1 (flow--instance-create :pipeline "pipeline"
                                           :number 666))
         (instance2 (flow--instance-create :pipeline "pipeline"
                                           :number 667))
         (delta1 (flow-delta-create instance1))
         (delta2 (flow-delta-create (list instance1 instance2))))
    (should (= (flow--delta-from delta1) (flow--instance-number instance1)))
    (should (= (flow--delta-to delta1) (flow--instance-number instance1)))
    (should (= (flow--delta-from delta2) (flow--instance-number instance1)))
    (should (= (flow--delta-to delta2) (flow--instance-number instance2)))))

(provide 'flow-test)
;;; flow-test.el ends here
